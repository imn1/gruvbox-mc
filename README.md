# Gruvbox theme for MOCP

<div align="center">
    <img src="https://gitlab.com/imn1/gruvbox-mc/-/raw/master/assets/screenshot.png">
</div>

## Installing
Just copy the theme file to `~/.local/share/mc/skins` directory.
Then run `mc` press `F9` -> Options -> Appearance -> Skin and
choose `gruvbox`.
